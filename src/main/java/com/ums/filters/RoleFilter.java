package com.ums.filters;

import com.ums.entities.UserDetailsImpl;
import com.ums.jwt.JwtUtils;
import com.ums.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.ums.entities.Role.ADMIN;
import static com.ums.filters.OperationPaths.AUTHENTICATE;
import static com.ums.filters.UriPaths.AUTHENTICATION;
import static com.ums.filters.UriPaths.USER;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public class RoleFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Override
    @Order(3)
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = jwtUtils.parseJwt(request);
            if (token != null) {
                String uri = request.getRequestURI();
                // resource path is the path after /api
                String uriPrefix = uri.substring(4, uri.lastIndexOf('/'));
                // operation path is the path after the last /
                String operation = request.getRequestURI().substring(uri.lastIndexOf('/'));
                String username = jwtUtils.getUserNameFromJwtToken(token);
                UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername(username);
                if (
                        ((USER.getPath().equals(uriPrefix)
                                || (AUTHENTICATION.getPath().equals(uriPrefix) && !AUTHENTICATE.getPath().equals(operation))
                        ) && !ADMIN.equals(userDetails.getRole()))) {
                    throw new HttpClientErrorException(UNAUTHORIZED);
                }
            }
            filterChain.doFilter(request, response);
        } catch (HttpClientErrorException e) {
            logger.warn("Unauthorized request");
            resolver.resolveException(request, response, null, e);
        }
    }
}
