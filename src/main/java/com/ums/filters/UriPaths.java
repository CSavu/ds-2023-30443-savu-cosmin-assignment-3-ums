package com.ums.filters;

public enum UriPaths {
    USER("/user"),
    AUTHENTICATION("/authentication");

    private String path;

    UriPaths(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
