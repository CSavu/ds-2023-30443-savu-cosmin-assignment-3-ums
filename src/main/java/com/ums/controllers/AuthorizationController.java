package com.ums.controllers;

import com.ums.jwt.JwtUtils;
import com.ums.responses.AuthorizationResponse;
import com.ums.services.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/authorization")
public class AuthorizationController {
    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/verifyToken")
    @ResponseStatus(HttpStatus.OK)
    public AuthorizationResponse verifyToken(HttpServletRequest request) {
        String headerAuth = jwtUtils.parseJwt(request);
        return authorizationService.verifyToken(headerAuth);
    }
}
