package com.ums.responses;

import com.ums.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthenticationResponse extends SimpleUserResponse {
    private String token;

    public AuthenticationResponse(String username, Role role, Integer userId, String token) {
        super(username, role, userId);
        this.token = token;
    }
}
