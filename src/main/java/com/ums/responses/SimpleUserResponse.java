package com.ums.responses;

import com.ums.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SimpleUserResponse {
    private String username;
    private Role role;
    private Integer userId;
}
