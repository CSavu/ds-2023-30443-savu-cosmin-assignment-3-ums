package com.ums.services;

import com.ums.entities.User;
import com.ums.jwt.JwtUtils;
import com.ums.responses.AuthorizationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationService {
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserService userService;

    public AuthorizationResponse verifyToken(String token) {
        boolean isTokenValid = jwtUtils.validateJwtToken(token);
        String username = jwtUtils.getUserNameFromJwtToken(token);
        User user = userService.getUserByUsername(username);
        return new AuthorizationResponse(
                username,
                user.getRole(),
                user.getId(),
                isTokenValid);
    }
}
