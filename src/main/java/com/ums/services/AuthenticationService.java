package com.ums.services;

import com.ums.entities.User;
import com.ums.jwt.JwtUtils;
import com.ums.requests.AuthenticationRequest;
import com.ums.responses.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;

    public AuthenticationResponse authenticate(AuthenticationRequest loginAuthenticationRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginAuthenticationRequest.getUsername(), loginAuthenticationRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtils.generateJwtToken(authentication);
        User user = userService.getUserByUsername(loginAuthenticationRequest.getUsername());

        return new AuthenticationResponse(user.getUsername(), user.getRole(), user.getId(), token);
    }
}
