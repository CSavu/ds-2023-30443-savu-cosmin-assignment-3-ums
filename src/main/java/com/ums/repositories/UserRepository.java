package com.ums.repositories;

import com.ums.entities.Role;
import com.ums.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    List<User> findUsersByRole(Role role);
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
}

