package com.ums.exceptions;

public class InvalidPropertyException extends RuntimeException {
    public InvalidPropertyException() {
        super("Invalid property of object!");
    }
}
