# UMS (User Management Service)

## Overview

UMS is the microservice responsible for the management of users within the Energy Management System.

## Getting Started

### Prerequisites

Make sure you have Java, Maven, and Docker installed on your machine.

- Java: [Download and Install Java](https://www.oracle.com/java/technologies/javase-downloads.html)
- Maven: [Download and Install Maven](https://maven.apache.org/download.cgi)
- Docker: [Download and Install Docker](https://www.docker.com/get-started)

### Installation

1. Clone the UMS repository to your local machine.

    ```bash
    git clone https://gitlab.com/CSavu/ums.git
    ```

2. Navigate to the project directory.

    ```bash
    cd ums
    ```

3. Build the application using Maven.

    ```bash
    mvn clean install
    ```

### Dockerization

Build the Docker image using the provided Dockerfile.

```bash
docker build -t ums .
```